/*
 * spi.c
 *
 *  Created on: Apr 12, 2018
 *      Author: Kunal Bhadane
 */

#include <motor.h>
#include <string.h>

#include "stm32l4xx_hal.h"
#include "spi.h"

SPI_HandleTypeDef hspi1;
HAL_SPI_StateTypeDef   status;        /* SPI communication state */

/* SPI1 init function */
void SPI_Init(void)
{
	/* SPI1 parameter configuration*/
	  hspi1.Instance = SPI1;
	  hspi1.Init.Mode = SPI_MODE_SLAVE;
	  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
	  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	  hspi1.Init.NSS = SPI_NSS_HARD_INPUT; //SPI_NSS_SOFT;
	  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	  hspi1.Init.CRCPolynomial = 7;
	  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
	  if (HAL_SPI_Init(&hspi1) != HAL_OK)
	  {
	    _Error_Handler(__FILE__, __LINE__);
	  }
	  while(hspi1.State!=HAL_SPI_STATE_READY) {}
	  //printf("SPI Ready\n");
}

/**
* @brief This function handles SPI1 global interrupt.
*/
void SPI1_IRQHandler(void)
{
	/*SPI interrupt handler*/
	HAL_SPI_IRQHandler(&hspi1);
	//printf("Interrupt\n");
	//SPI_Process();
}

/* - Function for transmitting and receiving data over SPI.
 * The function writes the current motor status (limit switch
 * status and moving status) in the transmit buffer and waits
 * for the SPI interrupt.
 * - As soon as the interrupt is received, the data transfer will
 * be performed. Hence, it is important to update the motor
 * status before data is written into transmit buffer.
 * - To get the current status, SPI_Process function is called after
 * actions are performed and BLE MCU requests 2 times for status
 * update.
 * - The transmit buffer will be updated every time before data
 * transfer. In this way, BLE mcu will receive motor status when
 * it transmits command and when it requests for motor status.
 */

void SPI_Process(void)
{
#if EXPANSION_MOTOR_ENABLE
	//aTxBuffer[0] = Get_Limit_Switch_Status() | exp_state;
	aTxBuffer[0] = Get_Limit_Switch_Status();
#elif ELEVATION_MOTOR_ENABLE
	//aTxBuffer[0] = Get_Limit_Switch_Status() | elev_state;
	aTxBuffer[0] = Get_Limit_Switch_Status();
#elif AZIMUTH_MOTOR_ENABLE
	aTxBuffer[0] = azi_state;
#endif

	if(hspi1.State == HAL_SPI_STATE_READY)
	{
		//printf("Send State = %x\n",aTxBuffer[0]);
		if(HAL_SPI_TransmitReceive_IT(&hspi1, aTxBuffer, aRxBuffer, TX_RX_BUFFER_SIZE) != HAL_OK)
		//if(HAL_SPI_TransmitReceive(&hspi1, aTxBuffer, aRxBuffer, sizeof(aTxBuffer), 5000) != HAL_OK)
		{
			//printf("Error\n");
		}
		//printf("Received = %x\n",aRxBuffer[0]);
	}
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	//printf("Transferred = %x\n",aTxBuffer[0]);
	//printf("Received = %x\n",aRxBuffer[0]);

	if(motor_INT_on){
		//update_status = false;
		motor_INT_on = false;
		HAL_GPIO_WritePin(MAIN_MCU_INT_GPIO_Port, MAIN_MCU_INT_Pin, GPIO_PIN_SET);
	}

	command_modified = true;
#if EXPANSION_MOTOR_ENABLE
	switch(aRxBuffer[0])
	{
		case exp_open:
			command = exp_open;
			break;
		case exp_close:
			command = exp_close;
			break;
		case exp_stop:
			command = exp_stop;
			//printf("Stop!\n");
			break;
		case exp_status:
			command = 0x00;
			break;
		default:
			break;
	}
#elif ELEVATION_MOTOR_ENABLE
	switch(aRxBuffer[0])
	{
		case tilt_down:
			command = tilt_down;
			break;
		case tilt_up:
			command = tilt_up;
			break;
		case tilt_stop:
			command = tilt_stop;
			break;
		case tilt_to:
			tilt_angle = aRxBuffer[1];
			command = tilt_to;
			break;
		case tilt_status:
			command = 0x00;
			break;
		default:
			break;
	}

#elif AZIMUTH_MOTOR_ENABLE

	if(aRxBuffer[1] == 0xAB)
	{
		switch(aRxBuffer[0])
		{
			case rotate_right:
				command = rotate_right;
				break;
			case rotate_left:
				command = rotate_left;
				break;
			case rotation_stop:
				command = rotation_stop;
				break;
			case calibrate:
				command = calibrate;
				break;
			case rotation_status:
				command = 0x00;
				break;
			default:
				break;
		}
	}
	else
	{
		rotate_angle = (aRxBuffer[1] << 8) + aRxBuffer[0];
		command = rotate_by;
	}
#endif
}

/* Function to update status to BLE. If update_status is true,
 * set the flag indicating the interrupt signal is ON, and clear
 * the MAIN MCU INT pin (generate signal).
 */
void Update_BLE_MCU(void)
{
	motor_INT_on = true;
	HAL_GPIO_WritePin(MAIN_MCU_INT_GPIO_Port, MAIN_MCU_INT_Pin, GPIO_PIN_RESET);
}

void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hspi->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspInit 0 */

  /* USER CODE END SPI1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SPI1_CLK_ENABLE();

    /**SPI1 GPIO Configuration
    PA5     ------> SPI1_SCK
    PA6     ------> SPI1_MISO
    PB0     ------> SPI1_NSS
    PA12     ------> SPI1_MOSI
    */
    GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


    /*Configure GPIO pin : SPI1_EXTI_NSS_Pin */
    //GPIO_InitStruct.Pin = SPI1_EXTI_NSS_Pin;
    //GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    //GPIO_InitStruct.Pull = GPIO_PULLUP;
    //HAL_GPIO_Init(SPI1_EXTI_NSS_GPIO_Port, &GPIO_InitStruct);

    /* SPI1 interrupt Init */
    HAL_NVIC_SetPriority(SPI1_IRQn, 3, 0);
    HAL_NVIC_EnableIRQ(SPI1_IRQn);

    //HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
    //HAL_NVIC_EnableIRQ(EXTI0_IRQn);
  }

}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{

  if(hspi->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspDeInit 0 */

  /* USER CODE END SPI1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI1_CLK_DISABLE();

    /**SPI1 GPIO Configuration
    PA5     ------> SPI1_SCK
    PA6     ------> SPI1_MISO
    PB0     ------> SPI1_NSS
    PA12     ------> SPI1_MOSI
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_12);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_0);

    /* SPI1 interrupt DeInit */
    HAL_NVIC_DisableIRQ(SPI1_IRQn);
  /* USER CODE BEGIN SPI1_MspDeInit 1 */

  /* USER CODE END SPI1_MspDeInit 1 */
  }

}

