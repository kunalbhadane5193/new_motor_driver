/*
 * gpio.c
 *
 *  Created on: Apr 11, 2018
 *      Author: Kunal Bhadane
 */

/* Includes ------------------------------------------------------------------*/
#include <motor.h>
#include "gpio.h"
#include "main.h"
#include "spi.h"
#include "encoder.h"
#include "stm32l4xx_hal.h"

/* Public function prototypes ------------------------------------------------*/
/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through
        * the Code Generation settings)
*/
void GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pins : PA4 PA7 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB3 PB4 PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PH3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
#if AZIMUTH_MOTOR_ENABLE
	if(GPIO_Pin == LIMIT_SWITCH_A_Pin)
	{
		halleffect_sensor_callback();
	}
#else
	switch(GPIO_Pin){
		case LIMIT_SWITCH_A_Pin:
			limit_switch_A_callback();
			break;
		case LIMIT_SWITCH_B_Pin:
			limit_switch_B_callback();
			break;
	}
#endif

	if(GPIO_Pin == MOTOR_ENC_1_Pin)
	{
		Encoder_Callback();
	}

	/*if(GPIO_Pin == SPI1_EXTI_NSS_Pin)
	{
		NSS_Callback();
	}*/
}
