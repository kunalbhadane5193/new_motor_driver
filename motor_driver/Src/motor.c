/*
 * motor.c
 *
 *  Created on: Apr 11, 2018
 *      Author: Kunal Bhadane
 */

#include <motor.h>
#include <stdbool.h>
#include <inttypes.h>

#include "stm32l4xx_hal.h"
#include "timer.h"
#include "pwm.h"
#include "encoder.h"
#include "spi.h"

void Motor_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, MOTOR_DIR_OUT_Pin|MOTOR_ENABLE_Pin|MOTOR_DISABLE_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, MAIN_MCU_INT_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, STATUS_LED_Pin|FAULT_LED_Pin, GPIO_PIN_SET);

	/*Configure GPIO pins : LIMIT_SWITCH_A_Pin LIMIT_SWITCH_B_Pin */
	GPIO_InitStruct.Pin = LIMIT_SWITCH_A_Pin|LIMIT_SWITCH_B_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : MOTOR_DIR_OUT_Pin MOTOR_ENABLE_Pin MOTOR_DISABLE_Pin*/
	GPIO_InitStruct.Pin = MOTOR_DIR_OUT_Pin|MOTOR_ENABLE_Pin|MOTOR_DISABLE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* Configure GPIO pins : MAIN_MCU_INT_Pin */
	GPIO_InitStruct.Pin = MAIN_MCU_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : MOTOR_FAULT_INPUT_Pin */
	GPIO_InitStruct.Pin = MOTOR_FAULT_INPUT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(MOTOR_FAULT_INPUT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : STATUS_LED_Pin FAULT_LED_Pin */
	GPIO_InitStruct.Pin = STATUS_LED_Pin|FAULT_LED_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : PA8 PA9 */
	GPIO_InitStruct.Pin = MOTOR_ENC_1_Pin|MOTOR_ENC_2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

/*Process Commands received over SPI from BLE MCU*/
#if EXPANSION_MOTOR_ENABLE
void Process_Expansion_Motor_Command(Expansion_Motor_Command mcommand)
{
	switch(mcommand)
	{
		case exp_open:
			//if(exp_state == expansion_stopped){
				Expansion_Open();
			//}
			break;
		case exp_close:
			//if(exp_state == expansion_stopped){
				Expansion_Close();
			//}
			break;
		case exp_stop:
			Motor_Stop();
			break;
		default:
			//Motor_Stop();
			break;
	}
}
void Expansion_Open(void)
{
	if(Get_Limit_Switch_Status() == limit_opened)
	{
		update_status = true;
		SPI_Process();
		return;
	}

	//printf("Open\n");

	if(Get_Limit_Switch_Status() == middle){

		Motor_Start();
		Motor_Set_Direction(POSITIVE);
		PWM_Set(75);
	}
	else
	{
		while(Get_Limit_Switch_Status() != middle)
		{
			//printf("Expansion Open\n");
			Motor_Start();
			Motor_Set_Direction(POSITIVE);
			PWM_Set(75);
		}
	}

	exp_state = expansion_opening;
	update_status = true;
}

void Expansion_Close(void)
{
	if(Get_Limit_Switch_Status() == limit_closed)
	{
		update_status = true;
		SPI_Process();
		return;
	}

	//printf("Close\n");

	if(Get_Limit_Switch_Status() == middle){
		Motor_Start();
		Motor_Set_Direction(NEGATIVE);
		PWM_Set(75);
	}
	else
	{
		while(Get_Limit_Switch_Status() != middle)
		{
			//printf("Expansion Close\n");
			Motor_Start();
			Motor_Set_Direction(NEGATIVE);
			PWM_Set(75);
		}
	}

	exp_state = expansion_closing;
	update_status = true;
}
#elif ELEVATION_MOTOR_ENABLE
void Process_Elevation_Motor_Command(Elevation_Motor_Command mcommand)
{
	switch(mcommand)
	{
		case tilt_down:
			Tilt_Down();
			break;
		case tilt_up:
			Tilt_Up();
			break;
		case tilt_to:
			if(tilt_angle == 0)
			{
				Tilt_Up();
			}
			else if(tilt_angle == 45)
			{
				Tilt_Down();
			}
			else
			{
				Tilt_To(tilt_angle);
			}
			break;
		case tilt_stop:
			Motor_Stop();
			break;
		default:
			break;
	}
}

void Tilt_Up(void)
{
	if(Get_Limit_Switch_Status() == limit_closed)
	{
		update_status = true;
		SPI_Process();
		return;
	}
	/* For tilting motor direction is opposite.
	 * Hence for tilting up motor direction should be
	 * positive.
	 */

	if(Get_Limit_Switch_Status() == middle){
		//printf("Up\n");
		Motor_Start();
		Motor_Set_Direction(POSITIVE);
		PWM_Set(75);
	}
	else
	{
		while(Get_Limit_Switch_Status() != middle)
		{
			//printf("Tilt Up\n");
			Motor_Start();
			Motor_Set_Direction(POSITIVE);
			PWM_Set(75);
		}
	}
	/* Set destination value to 0 as it will reach the limit switch
	 * and will update the position value to destination set.
	 */
	elev_state = tilting_up;
	update_status = true;
}

void Tilt_Down(void)
{
	if(Get_Limit_Switch_Status() == limit_opened)
	{
		update_status = true;
		SPI_Process();
		return;
	}
	/* For tilting motor direction is opposite
	 * Hence for tilting down motor direction should be
	 * Negative.
	 */

	//printf("Down\n");

	if(Get_Limit_Switch_Status() == middle){
		Motor_Start();
		Motor_Set_Direction(NEGATIVE);
		PWM_Set(75);
	}
	else
	{
		while(Get_Limit_Switch_Status() != middle)
		{
			//printf("Tilt Down\n");
			Motor_Start();
			Motor_Set_Direction(NEGATIVE);
			PWM_Set(75);
		}
	}
	/* Set destination value to MAX_POSITION as it will reach the limit switch
	 * and will update the position value to destination set.
	 */
	elev_state = tilting_down;
	update_status = true;
}

void Tilt_To(uint8_t angle)
{
	static uint16_t cur_position; // Variable to store current position

	/* If value of the angle instructed is different than the previous value
	 * or existing motor position, calculate new position to travel and allow
	 * motor to move to a new position.
	 * - If the angle is 0 or 45 ignore position calculations and let the motor move
	 * till it reaches the limit switch.
	 * - If an angle is between minimum and maximum value calculate the destination
	 * position.
	 */
	//printf("%d\n",angle);
	cur_position = Get_Elevation_Current_Position();
	//printf("%" PRId32 "\n",cur_position);
	/* Set current position as previous position
	 * which will be used to find distance traveled in position update function*/
	previous_position = cur_position;

	destination = angle * (int32_t)TICKS_PER_DEG;

	//distance_to_travel = destination - cur_position;

	/* If destination position is higher than the current position
	 * Move in forward direction (tilt down)*/
	if(destination > cur_position)
	{
		distance_to_travel = destination - cur_position;
		/*Motor_Start();
		Motor_Set_Direction(NEGATIVE);
		PWM_Set(50);
		*/
		Tilt_Down();
		//Encoder_Start();
		encoder_on = true;
		elev_state = tilting_down;
	}
	/* If destination position is lower than the current position
	 * Move in backward direction (tilt up)*/
	else if(destination < cur_position)
	{
		distance_to_travel = cur_position - destination;
		//Motor_Start();
		//Motor_Set_Direction(POSITIVE);
		//PWM_Set(50);
		//Encoder_Start();
		Tilt_Up();
		encoder_on = true;
		elev_state = tilting_up;
	}
	else
	{
		distance_to_travel = 0;
		Motor_Stop();
	}

	//printf("%" PRId32 "\n",distance_to_travel);
}

uint16_t Get_Elevation_Current_Position(void)
{
	Limit_Switch_State limit_switch = Get_Limit_Switch_Status();

	if(limit_switch == limit_closed)
	{
		current_position = 0;
	}
	else if(limit_switch == limit_opened)
	{
		current_position = MAX_POSITION;
	}
	else
	{
		/* Position is updated when motor stopped and current position
		 * is the recently updated position value when motor stopped.
		 */
		current_position = position;
	}
	return current_position;
}
#elif AZIMUTH_MOTOR_ENABLE
void Process_Azimuth_Motor_Command(Azimuth_Motor_Command mcommand)
{
	switch(mcommand)
	{
		case rotate_right:
			Rotate_Right();
			break;
		case rotate_left:
			Rotate_Left();
			break;
		case rotation_stop:
			Motor_Stop();
			break;
		case rotate_by:
			Rotate_By(rotate_angle);
			break;
		case calibrate:
			Calibrate_Motor();
			break;
		default:
			break;
	}
}

void Rotate_Right(void)
{
	//printf("Rotate Right\n");
	//Encoder_Start();
	PWM_Set(75);
	Motor_Start();
	Motor_Set_Direction(NEGATIVE);
	azi_state = rotating_right;
	update_status = true;
}

void Rotate_Left(void)
{
	//printf("Rotate Left\n");
	//Encoder_Start();
	PWM_Set(75);
	Motor_Start();
	Motor_Set_Direction(POSITIVE);
	azi_state = rotating_left;
	update_status = true;
}

/* Currently calibration part makes azimuth motor to find magnet
 * which is a home position for azimuth motor. It is necessary as
 * it will be easy to start rotation to any other angle from home
 * position.
 */
void Calibrate_Motor(void)
{
	//printf("Calibration Start\n");
	if(!motor_calibrating)
	{
		//printf("Calibration Start\n");
		motor_calibrating = true;
		Rotate_Right();
		update_status = true;
	}
}

void Rotate_By(uint16_t angle)
{
//	/* Make encoder count always 0 before starting rotation*/
//	//TIM1->CNT = 0;
//
//	/* Current position will always be 0 before stating rotation.
//	 * The exact position of azimuth motor not needed.
//	 */
//	//current_position = 0;
//
//	//previous_position = current_position;
//
//
//	/* Find out the distance to travel from the input angle.
//	 * If angle is positive rotate right else rotate left.
//	 */
//	if(angle > 0)
//	{
//		distance_to_travel = (int32_t)(angle * TICKS_PER_DEG);
//		Rotate_Right();
//		encoder_on = true;
//	}
//
//	else if(angle < 0)
//	{
//		distance_to_travel = (int32_t)((-angle) * TICKS_PER_DEG);
//		Rotate_Left();
//		encoder_on = true;
//	}
//
//	else
//	{
//		distance_to_travel = 0;
//		Motor_Stop();
//	}
//
//	//printf("%" PRId32 "\n",distance_to_travel);
//
//	/* Destination position will always be 0. When motor stops,
//	 * position value will be set by this.
//	 */
//	destination = 0;

	//printf("%d\n",angle);

	destination = (uint16_t)(angle * TICKS_PER_DEG);

	if(destination > current_position)
	{
		distance_to_travel = destination - current_position;
		// Find shortest path to travel
		if(distance_to_travel > TICKS_180_DEG)
		{
			distance_to_travel = MAX_POSITION - distance_to_travel;
			Rotate_Left();
		}
		else
		{
			Rotate_Right();
		}
		//Rotate_Right();
		encoder_on = true;
	}
	else if(destination < current_position)
	{
		distance_to_travel = current_position - destination;
		// Find shortest path to travel
		if(distance_to_travel > TICKS_180_DEG)
		{
			distance_to_travel = MAX_POSITION - distance_to_travel;
			Rotate_Right();
		}
		else
		{
			Rotate_Left();
		}
		//Rotate_Left();
		encoder_on = true;
	}
	else
	{
		distance_to_travel = 0;
		Motor_Stop();
	}
	//printf("%d\n",distance_to_travel);
}
#endif

void Motor_Start(void)
{
	/*Enable motor driver*/
	PWM_Start();
	HAL_GPIO_WritePin(MOTOR_ENABLE_GPIO_Port, MOTOR_ENABLE_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOTOR_DISABLE_GPIO_Port, MOTOR_DISABLE_Pin, GPIO_PIN_RESET);
}

void Motor_Stop(void)
{
	encoder_on = false;
	/*Disable motor driver*/
	//HAL_GPIO_WritePin(MOTOR_ENABLE_GPIO_Port, MOTOR_ENABLE_Pin, GPIO_PIN_RESET);
	//HAL_GPIO_WritePin(MOTOR_DISABLE_GPIO_Port, MOTOR_DISABLE_Pin, GPIO_PIN_SET);
	PWM_Set(0);
	PWM_Stop();
	Timer_Stop();
	//printf("%d\n",current_count);
	//current_count = 0;
#if EXPANSION_MOTOR_ENABLE
	exp_state = expansion_stopped;
#elif ELEVATION_MOTOR_ENABLE
	elev_state = tilting_stopped;
#elif AZIMUTH_MOTOR_ENABLE
	azi_state = rotation_stopped;
	if(calibration_done)
	{
		calibration_done = false;
		azi_state = calibration_complete;
	}
#endif

	update_status = true;
	SPI_Process();
}

void Motor_Set_Direction(Motor_Direction direction)
{
	switch(direction)
	{
		case NEGATIVE:
			HAL_GPIO_WritePin(MOTOR_DIR_OUT_GPIO_Port, MOTOR_DIR_OUT_Pin, GPIO_PIN_RESET);
			break;
		case POSITIVE:
			HAL_GPIO_WritePin(MOTOR_DIR_OUT_GPIO_Port, MOTOR_DIR_OUT_Pin, GPIO_PIN_SET);
			break;
		default:
			break;
	}
}

void LED_ON(LED led)
{
	switch(led)
	{
		case status_led:
			HAL_GPIO_WritePin(STATUS_LED_GPIO_Port, STATUS_LED_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FAULT_LED_GPIO_Port, FAULT_LED_Pin, GPIO_PIN_SET);
			led_on = true;
			break;
		case fault_led:
			HAL_GPIO_WritePin(FAULT_LED_GPIO_Port, FAULT_LED_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(STATUS_LED_GPIO_Port, STATUS_LED_Pin, GPIO_PIN_SET);
			led_on = true;
			break;
		default:
			break;
	}
}

void LED_OFF(void)
{
	HAL_GPIO_WritePin(STATUS_LED_GPIO_Port, STATUS_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(FAULT_LED_GPIO_Port, FAULT_LED_Pin, GPIO_PIN_SET);
	led_on = false;
}

#if AZIMUTH_MOTOR_ENABLE
void halleffect_sensor_callback(void)
{
	GPIO_PinState closed = HAL_GPIO_ReadPin(LIMIT_SWITCH_A_GPIO_Port, LIMIT_SWITCH_A_Pin);
	if(closed == 0)
	{
		if(motor_calibrating)
		{
			motor_calibrating = false;
			current_position = 0;
			calibration_done = true;
			command_modified = true;
			command = rotation_stop;
		}
		/*else
		{
			if(azi_state == rotating_right)
			{
				current_position = 0;
			}
			else if(azi_state == rotating_left)
			{
				current_position = MAX_POSITION;
			}
		}*/
	}
}

Limit_Switch_State Get_Sensor_Data(void)
{
	GPIO_PinState closed = HAL_GPIO_ReadPin(LIMIT_SWITCH_A_GPIO_Port, LIMIT_SWITCH_A_Pin);

	if(closed == 0)
	{
		return limit_closed;
	}
	else
		return limit_opened;
}
#else
void limit_switch_A_callback(void){
	GPIO_PinState closed = HAL_GPIO_ReadPin(LIMIT_SWITCH_A_GPIO_Port, LIMIT_SWITCH_A_Pin);
	//printf("%d\n",closed);
	if(closed == 1){
		key_released = true;
		key_pressed = false;
		LED_OFF();
		Timer_Start();
		return;
	}
	else if(closed == 0){
		key_pressed = true;
		key_released = false;
		Timer_Start();
		LED_ON(status_led);
/*		command_modified = true;
#if EXPANSION_MOTOR_ENABLE
		command = exp_stop;
#elif ELEVATION_MOTOR_ENABLE
		command = tilt_stop;
#endif
*/
	}
}

void limit_switch_B_callback(void){
	GPIO_PinState opened = HAL_GPIO_ReadPin(LIMIT_SWITCH_B_GPIO_Port, LIMIT_SWITCH_B_Pin);

	if(opened == 1){
		key_released = true;
		key_pressed = false;
		LED_OFF();
		Timer_Start();
		return;
	}
	else if(opened == 0){
		key_pressed = true;
		key_released = false;
		Timer_Start();
		LED_ON(fault_led);

		/*command_modified = true;
#if EXPANSION_MOTOR_ENABLE
		command = exp_stop;
#elif ELEVATION_MOTOR_ENABLE
		command = tilt_stop;
#endif
*/
	}
}

Limit_Switch_State Get_Limit_Switch_Status(void)
{
	GPIO_PinState closed = HAL_GPIO_ReadPin(LIMIT_SWITCH_A_GPIO_Port, LIMIT_SWITCH_A_Pin);
	GPIO_PinState opened = HAL_GPIO_ReadPin(LIMIT_SWITCH_B_GPIO_Port, LIMIT_SWITCH_B_Pin);


	if(closed == 0)
	{
		//printf("Closed = %d\n",closed);
		return limit_closed;
	}
	else if(opened == 0)
	{
		//printf("Opened = %d\n",opened);
		return limit_opened;
	}
	else
	{
		return middle;
	}
}
#endif

