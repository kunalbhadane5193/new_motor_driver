///*
// * encoder.c
// *
// *  Created on: Apr 18, 2018
// *      Author: Kunal Bhadane
// */
//
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include "stm32l4xx_hal.h"

#include "timer.h"
#include "encoder.h"
#include "motor.h"
//
//TIM_HandleTypeDef henc;
//
///* TIM1 init function */
//void Encoder_Init(void)
//{
//
//  TIM_Encoder_InitTypeDef sConfig;
//  TIM_MasterConfigTypeDef sMasterConfig;
//
//  henc.Instance = TIM1;
//  henc.Init.Prescaler = 0;
//  henc.Init.CounterMode = TIM_COUNTERMODE_UP;
//  henc.Init.Period = 65535;
//  henc.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//  henc.Init.RepetitionCounter = 0;
//  henc.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
//  sConfig.EncoderMode = TIM_ENCODERMODE_TI1;
//  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
//  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
//  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
//  sConfig.IC1Filter = 0x0F;
//  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
//  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
//  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
//  sConfig.IC2Filter = 0x0F;
//  if (HAL_TIM_Encoder_Init(&henc, &sConfig) != HAL_OK)
//  {
//    _Error_Handler(__FILE__, __LINE__);
//  }
//
//  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
//  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
//  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
//  if (HAL_TIMEx_MasterConfigSynchronization(&henc, &sMasterConfig) != HAL_OK)
//  {
//    _Error_Handler(__FILE__, __LINE__);
//  }
//
//  TIM1->CNT = 0;
//
//  current_count = 0;
//  previous_count = 0;
//
//  current_position = 0;
//  previous_position = 0;
//
//}
//
//void Encoder_Start(void)
//{
//#if EXPNASION_MOTOR_ENABLE
//	return;
//#else
//	if(HAL_TIM_Encoder_Start(&henc, TIM_CHANNEL_1) != HAL_OK){
//		_Error_Handler(__FILE__, __LINE__);
//	}
//
//#endif
//}
//
//void Encoder_Stop(void)
//{
//#if EXPNASION_MOTOR_ENABLE
//	return;
//#else
//	if(HAL_TIM_Encoder_Stop(&henc, TIM_CHANNEL_1) != HAL_OK){
//		_Error_Handler(__FILE__, __LINE__);
//	}
//	encoder_on = false;
//
//	/* When motor and encoder stops, get the current position.
//	 * Use this position value to determine current position value
//	 * when next command is received to tilt/rotate.
//	 */
//	//current_count = TIM1->CNT;
//	//current_position = current_count;
//	position = destination;
//	//printf("%" PRId32 "\n",current_position);
//	//printf("%d\n",current_count);
//#endif
//}
//
///* Update the current position in Timer callback every 5ms*/
//void Position_Update(void)
//{
//	static int16_t delta_count = 0;
//
//	//current_count = 0;
//
//	current_count = TIM1->CNT;//henc.Instance->CNT;
//
//	//current_count = __HAL_TIM_GET_COUNTER(&henc);
//
//	delta_count = current_count - previous_count;
//
//	previous_count = current_count;
//
//	current_position = current_position + (int32_t)delta_count; //current_count;
//
//#if ELEVATION_MOTOR_ENABLE
//	/* Knowing Moving direction will help us to get positive value of the distance traveled*/
//	if(elev_state == tilting_down)
//	{
//		distance_traveled = current_position - previous_position;
//	}
//	else if(elev_state == tilting_up)
//	{
//		distance_traveled = previous_position - current_position;
//	}
//	else
//	{
//		distance_traveled = 0;
//	}
//
//	/* Check if it reached to destination*/
//	if(distance_traveled >= distance_to_travel)
//	{
//		//printf("%" PRId32 "\n",distance_traveled);
//		distance_traveled = 0;
//		//Motor_Stop();
//		command_modified = true;
//		command = tilt_stop;
//		current_count = 0;
//		previous_count = 0;
//	}
//
//	/*if(current_count == distance_to_travel)
//	{
//		command_modified = true;
//		command = tilt_stop;
//		distance_traveled = 0;
//		current_count = 0;
//		TIM1->CNT = 0;
//		//Motor_Stop();
//	}*/
//
//#elif AZIMUTH_MOTOR_ENABLE
//	if(azi_state == rotating_right)
//	{
//		distance_traveled = current_position - previous_position;
//	}
//	else if(azi_state == rotating_left)
//	{
//		distance_traveled = previous_position - current_position;
//	}
//	else
//	{
//		distance_traveled = 0;
//		Motor_Stop();
//	}
//
//#endif
//}
//
//#if ELEVATION_MOTOR_ENABLE
//int32_t Get_Elevation_Current_Position(void)
//{
//	Limit_Switch_State limit_switch = Get_Limit_Switch_Status();
//
//	if(limit_switch == limit_closed)
//	{
//		current_position = 0;
//		//TIM1->CNT = 0;
//		//tilt_angle = 0;
//	}
//	else if(limit_switch == limit_opened)
//	{
//		current_position = MAX_POSITION;
//		//TIM1->CNT = 0;
//		//tilt_angle = 45;
//	}
//	else
//	{
//		/* Position is updated when motor stopped and current position
//		 * is the recently updated position value when motor stopped.
//		 */
//		current_position = position;
//	}
//
//	//printf("%" PRId32 "\n",current_position);
//	return current_position;
//}
//#endif
//
//void HAL_TIM_Encoder_MspInit(TIM_HandleTypeDef* htim_encoder)
//{
//
//  GPIO_InitTypeDef GPIO_InitStruct;
//  if(htim_encoder->Instance==TIM1)
//  {
//  /* USER CODE BEGIN TIM1_MspInit 0 */
//
//  /* USER CODE END TIM1_MspInit 0 */
//    /* Peripheral clock enable */
//    __HAL_RCC_TIM1_CLK_ENABLE();
//
//    /* Enable GPIO Channels Clock */
//     __HAL_RCC_GPIOA_CLK_ENABLE();
//
//    /**TIM1 GPIO Configuration
//    PA8     ------> TIM1_CH1
//    PA9     ------> TIM1_CH2
//    */
//    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
//    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
//    GPIO_InitStruct.Pull = GPIO_PULLUP;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//    GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
//    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
//  /* USER CODE BEGIN TIM1_MspInit 1 */
//
//  /* USER CODE END TIM1_MspInit 1 */
//  }
//
//}
//
//void HAL_TIM_Encoder_MspDeInit(TIM_HandleTypeDef* htim_encoder)
//{
//
//  if(htim_encoder->Instance==TIM1)
//  {
//  /* USER CODE BEGIN TIM1_MspDeInit 0 */
//
//  /* USER CODE END TIM1_MspDeInit 0 */
//    /* Peripheral clock disable */
//    __HAL_RCC_TIM1_CLK_DISABLE();
//
//    /**TIM1 GPIO Configuration
//    PA8     ------> TIM1_CH1
//    PA9     ------> TIM1_CH2
//    */
//    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_8|GPIO_PIN_9);
//
//  /* USER CODE BEGIN TIM1_MspDeInit 1 */
//
//  /* USER CODE END TIM1_MspDeInit 1 */
//  }
//
//}
//
//

void Encoder_Callback(void)
{
	if(encoder_on)
	{
		++current_count;
		//--distance_to_travel;
		if(current_count >= distance_to_travel)
		{
			position_update = true;
		}
	}
}
