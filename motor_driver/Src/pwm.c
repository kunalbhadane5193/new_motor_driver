/*
 * pwm.c
 *
 *  Created on: Apr 11, 2018
 *      Author: Kunal Bhadane
 */

#include <motor.h>
#include "pwm.h"
#include "gpio.h"
#include "stm32l4xx_hal.h"

TIM_HandleTypeDef hpwm;

/* Private function prototypes -----------------------------------------------*/
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim);

/* Initialize PWM configurations
 * Timer2 channel 2 is used for PWM.
 * Initial PWM period set to 8000 for 10kHz PWM frequency at 80mHz clock.
 * (80 * 10 ^ 6)/8000 = 10 * 10 ^ 3 Hz (PWM Frequency).
 * As we need max frequency for timer, prescaler is set to 0.
 * Calculations for prescaler:
 * timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)
 * source: https://stm32f4-discovery.net/2014/05/stm32f4-stm32f429-discovery-pwm-tutorial/
 */
void PWM_Init(void){
	TIM_MasterConfigTypeDef sMasterConfig;
	TIM_OC_InitTypeDef sConfigOC;

	hpwm.Instance = TIM2;
	hpwm.Init.Prescaler = 0;
	hpwm.Init.CounterMode = TIM_COUNTERMODE_UP;
	hpwm.Init.Period = PWM_PERIOD_INIT;
	hpwm.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	hpwm.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_PWM_Init(&hpwm) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&hpwm, &sMasterConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = PWM_WIDTH_INIT;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&hpwm, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	HAL_TIM_MspPostInit(&hpwm);
}

/* PWM Start Function */
void PWM_Start(void) {
  if (HAL_TIM_PWM_Start(&hpwm, TIM_CHANNEL_2) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* PWM Stop Function */
void PWM_Stop(void) {
  if (HAL_TIM_PWM_Stop(&hpwm, TIM_CHANNEL_2) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* PWM Power Set Function */
void PWM_Set(float power) {

	//Setting up minimum PWM percentage
	static float min_pwm = 20.0;

  // Bounds limit
  if (power < 0.0) {
    power = 0.0;
  }
  // Check for minimum PWM for motor operation
  else if (power < min_pwm) {
    power = min_pwm;
  }
  else if (power > 100.0) {
    power = 100.0;
  }

  // Load new value
  hpwm.Instance->CCR2 = (uint32_t)(power * (float)PWM_PERIOD_INIT / 100.0);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{

  if(htim_pwm->Instance==TIM2)
  {
  /* USER CODE BEGIN TIM2_MspInit 0 */

  /* USER CODE END TIM2_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_TIM2_CLK_ENABLE();
  /* USER CODE BEGIN TIM2_MspInit 1 */

  /* USER CODE END TIM2_MspInit 1 */
  }

}

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  if(htim->Instance==TIM2)
  {
  /* USER CODE BEGIN TIM2_MspPostInit 0 */

  /* USER CODE END TIM2_MspPostInit 0 */

    /**TIM2 GPIO Configuration
    PA1     ------> TIM2_CH2
    */
    GPIO_InitStruct.Pin = MOTOR_PWM_OUT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
    HAL_GPIO_Init(MOTOR_PWM_OUT_GPIO_Port, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM2_MspPostInit 1 */

  /* USER CODE END TIM2_MspPostInit 1 */
  }

}

void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* htim_pwm)
{

  if(htim_pwm->Instance==TIM2)
  {
  /* USER CODE BEGIN TIM2_MspDeInit 0 */

  /* USER CODE END TIM2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM2_CLK_DISABLE();
  /* USER CODE BEGIN TIM2_MspDeInit 1 */

  /* USER CODE END TIM2_MspDeInit 1 */
  }

}
