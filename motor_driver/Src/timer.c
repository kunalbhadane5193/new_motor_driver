/*
 * timer.c
 *
 *  Created on: Apr 12, 2018
 *      Author: User
 */

/* Includes ------------------------------------------------------------------*/
#include "motor.h"
#include <stdbool.h>

#include "timer.h"
#include "pwm.h"
#include "encoder.h"
#include "stm32l4xx_hal.h"

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef h_timer;
uint8_t timer = 0;
uint8_t release_timer = 0;

/* Private function prototypes -----------------------------------------------*/


/* Public function implementation --------------------------------------------*/

/* Main timer init function */
void Timer_Init(void) {

  /* Calculate period based on clock frequency and desired cycle count *
   * Use PCLK1 frequency as time basis                                 */
  uint32_t tim_period = ((HAL_RCC_GetPCLK1Freq()/ 1000) * CYCLE_PERIOD_MS);
  uint32_t tim_prescaler = 1;

  /* Dynamically calculate period and prescaler */
  while (tim_period >= 65536) {
    tim_period = tim_period >> 1;
    tim_prescaler = tim_prescaler << 1;
  }

  TIM_MasterConfigTypeDef sMasterConfig;

  h_timer.Instance = TIM6;
  h_timer.Init.Prescaler = tim_prescaler - 1;
  h_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
  h_timer.Init.Period = tim_period;
  h_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&h_timer) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&h_timer, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* Timer start function */
void Timer_Start(void) {
  if (HAL_TIM_Base_Start_IT(&h_timer) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* Timer stop function */
void Timer_Stop(void) {
  if (HAL_TIM_Base_Stop_IT(&h_timer) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
  timer = 0;
  release_timer = 0;
}

/* Timer tick callback function */
void Timer_Tick_Callback(void)
{
	if(key_pressed)
	{
		timer++;
		release_timer = 0;

		if(timer >= 5){
			key_pressed = false;
			Timer_Stop();
			command_modified = true;
	#if EXPANSION_MOTOR_ENABLE
			command = exp_stop;
	#elif ELEVATION_MOTOR_ENABLE
			command = tilt_stop; //Elevation motor stop command
	#endif
			LED_OFF();
			//PWM_Set(0);
			//Motor_Stop();
		}
	}
	else if(key_released)
	{
		timer = 0;
		release_timer++;

		if(release_timer >= 20)
		{
			Timer_Stop();
			LED_OFF();
			key_released = false;
		}
	}

	/*if(encoder_on)
	{
		//Position_Update();
		position_update = true;
	}*/
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{

  if(htim_base->Instance==TIM6)
  {
     /* Peripheral clock enable */
    __HAL_RCC_TIM6_CLK_ENABLE();
    /* TIM6 interrupt Init */
    HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  }

}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base)
{

  if(htim_base->Instance==TIM6)
  {
    /* Peripheral clock disable */
    __HAL_RCC_TIM6_CLK_DISABLE();

    /* TIM6 interrupt DeInit */
    HAL_NVIC_DisableIRQ(TIM6_DAC_IRQn);
  }

}

/* TIM Trigger callback function */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
  if (htim->Instance == h_timer.Instance) {
    Timer_Tick_Callback();
  }
}


