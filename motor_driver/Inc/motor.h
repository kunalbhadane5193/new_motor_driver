/*
 * motor.h
 *
 *  Created on: Apr 11, 2018
 *      Author: Kunal Bhadane
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include <stdbool.h>
#include "stm32l4xx_hal.h"

/*Configure all motor GPIO*/

#define LIMIT_SWITCH_A_Pin GPIO_PIN_14
#define LIMIT_SWITCH_A_GPIO_Port GPIOC
#define LIMIT_SWITCH_A_EXTI_IRQn EXTI15_10_IRQn

#define LIMIT_SWITCH_B_Pin GPIO_PIN_15
#define LIMIT_SWITCH_B_GPIO_Port GPIOC
#define LIMIT_SWITCH_B_EXTI_IRQn EXTI15_10_IRQn

#define MOTOR_DIR_OUT_Pin GPIO_PIN_0
#define MOTOR_DIR_OUT_GPIO_Port GPIOA

#define MOTOR_ENABLE_Pin GPIO_PIN_10
#define MOTOR_ENABLE_GPIO_Port GPIOA

#define MOTOR_DISABLE_Pin GPIO_PIN_11
#define MOTOR_DISABLE_GPIO_Port GPIOA

#define STATUS_LED_Pin GPIO_PIN_6
#define STATUS_LED_GPIO_Port GPIOB

#define FAULT_LED_Pin GPIO_PIN_7
#define FAULT_LED_GPIO_Port GPIOB

#define MOTOR_FAULT_INPUT_Pin GPIO_PIN_3
#define MOTOR_FAULT_INPUT_GPIO_Port GPIOA
#define MOTOR_FAULT_INPUT_EXTI_IRQn EXTI3_IRQn

#define MAIN_MCU_INT_Pin GPIO_PIN_15
#define MAIN_MCU_INT_GPIO_Port GPIOA

#define MOTOR_ENC_1_Pin GPIO_PIN_8
#define MOTOR_ENC_1_GPIO_Port GPIOA

#define MOTOR_ENC_2_Pin GPIO_PIN_9
#define MOTOR_ENC_2_GPIO_Port GPIOA

//#define SPI1_EXTI_NSS_Pin GPIO_PIN_0
//#define SPI1_EXTI_NSS_GPIO_Port GPIOB
/* Motor Direction*/
typedef enum {
  NEGATIVE,             /* Positive direction */
  POSITIVE              /* Negative direction */
} Motor_Direction;

typedef enum {
	status_led,
	fault_led
}LED;

typedef enum {
	limit_closed = 0x01,
	limit_opened = 0x02,
	middle = 0x03
}Limit_Switch_State;

Limit_Switch_State limit_state;

#if EXPANSION_MOTOR_ENABLE
typedef enum {
	exp_open 	= 0x01,
	exp_close 	= 0x02,
	exp_stop 	= 0x03,
	exp_status  = 0xAA
}Expansion_Motor_Command;

typedef enum {
	expansion_opening = 0xA0,
	expansion_closing = 0xB0,
	expansion_stopped = 0xC0
}Expansion_Motor_State;

Expansion_Motor_State exp_state; 	/* Expansion Motor State */
Expansion_Motor_Command command;    /* Command for Motor */
void Process_Expansion_Motor_Command(Expansion_Motor_Command mcommand);

#elif ELEVATION_MOTOR_ENABLE
typedef enum {
	tilt_down   = 0x04,
	tilt_up 	= 0x05,
	tilt_stop 	= 0x06,
	tilt_to 	= 0x07,
	tilt_status = 0xAA
}Elevation_Motor_Command;


typedef enum {
	tilting_down 	= 0xD0,
	tilting_up		= 0xE0,
	tilting_stopped = 0xF0
}Elevation_Motor_State;

uint8_t tilt_angle;
Elevation_Motor_State elev_state; 	/* Elevation Motor State */
Elevation_Motor_Command command;    				/* Command for Motor */

void Process_Elevation_Motor_Command(Elevation_Motor_Command mcommand);

#elif AZIMUTH_MOTOR_ENABLE
typedef enum {
	rotate_right 		= 0x08,
	rotate_left 		= 0x09,
	rotation_stop 		= 0x10,
	rotate_by 			= 0x20,
	calibrate			= 0x30,
	rotation_status  	= 0xAA
}Azimuth_Motor_Command;

typedef enum {
	rotating_right 		= 0x60,
	rotating_left		= 0x70,
	rotation_stopped 	= 0x80,
	calibration_complete = 0x90
}Azimuth_Motor_State;

int16_t rotate_angle;
Azimuth_Motor_State azi_state;
Azimuth_Motor_Command command;    				/* Command for Motor */
bool motor_calibrating;
bool calibration_done;

void Process_Azimuth_Motor_Command(Azimuth_Motor_Command mcommand);
void Calibrate_Motor(void);
#endif

bool key_pressed;			/* Flag to detect if limit switch is pressed*/
bool key_released;			/* Flag to detect if limit switch is released*/
bool led_on;				/* Flag for led on detection*/
bool motor_INT_on;			/* Flag to indicate interrupt signal for BLE is activated*/
bool command_modified; 		/* To Check if new command received*/

void Motor_GPIO_Init(void);
void Motor_Start(void);
void Motor_Stop(void);
void Motor_Set_Direction(Motor_Direction direction);
void limit_switch_A_callback(void);
void limit_switch_B_callback(void);
void halleffect_sensor_callback(void);
void LED_ON(LED led);
void LED_OFF(void);

/*Expansion Motor functions*/
void Expansion_Open(void);
void Expansion_Close(void);

/*Elevation Motor functions*/
void Tilt_Up(void);
void Tilt_Down(void);
void Tilt_To(uint8_t angle);

/*Elevation Motor functions*/
void Rotate_Right(void);
void Rotate_Left(void);
void Rotate_By(uint16_t angle);

/*Function for limit switch status*/
Limit_Switch_State Get_Limit_Switch_Status(void);
Limit_Switch_State Get_Sensor_Data(void);

/*Functio to check limit switch status and motor status*/
void Update_Motor_Status(bool update);

#endif /* MOTOR_H_ */
