/*
 * pwm.h
 *
 *  Created on: Apr 11, 2018
 *      Author: Kunal Bhadane
 */

#ifndef PWM_H_
#define PWM_H_

#define MOTOR_PWM_OUT_Pin GPIO_PIN_1
#define MOTOR_PWM_OUT_GPIO_Port GPIOA

/* Private define ------------------------------------------------------------*/
#define PWM_WIDTH_INIT  0
#define PWM_PERIOD_INIT (8000) /*8000 count selected for 10kHz PWM frequency at 80mHz clock*/

void PWM_Init(void);
void PWM_Start(void);
void PWM_Stop(void);
void PWM_Set(float power);


#endif /* PWM_H_ */
