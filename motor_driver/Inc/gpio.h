/*
 * gpio.h
 *
 *  Created on: Apr 11, 2018
 *      Author: Kunal Bhadane
 */

#ifndef GPIO_H_
#define GPIO_H_

#include <stdbool.h>

#include "stm32l4xx_hal.h"

#define MOTOR_I_REF_Pin GPIO_PIN_2
#define MOTOR_I_REF_GPIO_Port GPIOA

#define VIN_REF_Pin GPIO_PIN_1
#define VIN_REF_GPIO_Port GPIOB

/* Public function prototypes ------------------------------------------------*/
void GPIO_Init(void);

#endif /* GPIO_H_ */
