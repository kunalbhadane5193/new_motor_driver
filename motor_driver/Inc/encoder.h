/*
 * encoder.h
 *
 *  Created on: Apr 18, 2018
 *      Author: Kunal Bhadane
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include <stdbool.h>
#include <stdint.h>
#include "stm32l4xx_hal.h"

volatile uint16_t current_count;
volatile uint16_t previous_count;
volatile uint16_t current_position;
volatile uint16_t previous_position;

uint16_t position;
uint16_t distance_traveled;
uint16_t distance_to_travel; // Variable to store distance to travel
uint16_t destination;  // Variable to store destination position

bool encoder_on;
bool position_update;

void Encoder_Init(void);
void Encoder_Start(void);
void Encoder_Stop(void);
void Position_Update(void);
uint16_t Get_Elevation_Current_Position(void);
void Encoder_Callback(void);
#endif /* ENCODER_H_ */
