/*
 * timer.h
 *
 *  Created on: Apr 12, 2018
 *      Author: User
 */

#ifndef TIMER_H_
#define TIMER_H_

/* Private define ------------------------------------------------------------*/

/* Public function prototypes ------------------------------------------------*/
void Timer_Init(void);
void Timer_Start(void);
void Timer_Stop(void);
void Timer_Tick_Callback(void);

extern void _Error_Handler(char *, int);


#endif /* TIMER_H_ */
