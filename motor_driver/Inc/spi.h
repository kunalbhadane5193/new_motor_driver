/*
 * spi.h
 *
 *  Created on: Apr 12, 2018
 *      Author: Kunal Bhadane
 */

#ifndef SPI_H_
#define SPI_H_

#include <motor.h>
#include <stdbool.h>
#include <stdint.h>
#include "stm32l4xx_hal.h"

#define TX_RX_BUFFER_SIZE 2
uint8_t aTxBuffer[TX_RX_BUFFER_SIZE];
uint8_t aRxBuffer[TX_RX_BUFFER_SIZE];

bool update_status;

/* Private define ------------------------------------------------------------*/
enum {
	TRANSFER_WAIT,
	TRANSFER_COMPLETE,
	TRANSFER_ERROR
};

void SPI_Init(void);
void SPI_Process(void);
//void NSS_Callback(void);
void Process_RX_Data(void);
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi);
//void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi);
//void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi);
//void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi);
//void Update_BLE_MCU(Expansion_Motor_State mstate);
void Update_BLE_MCU(void);

#endif /* SPI_H_ */
